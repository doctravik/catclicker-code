var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var imageResize = require('gulp-image-resize');

gulp.task('compile-font-awesome', function() {
    gulp.src('src/fonts/font-awesome-4.6.3/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS().on('error', sass.logError))
        .pipe(gulp.dest('dist/css/font-awesome'));
});

gulp.task('copy-font-awesome', function() {
    gulp.src('src/fonts/font-awesome-4.6.3/fonts/*.*')
        .pipe(gulp.dest('dist/fonts/font-awesome'));
});

gulp.task('copy-images', function() {
    gulp.src('src/img/**/*.*')
        .pipe(gulp.dest('dist/img'));
});

gulp.task('resize-images', function() {
    gulp.src(['src/img/cats/*.*', 'src/img/coffee-cat-640.png'])
        .pipe(imageResize({
            imageMagick: true,
            width : 150,
    }))
    .pipe(gulp.dest('dist/img/avatars'));
});

gulp.task('copy-index', function() {
    gulp.src('src/index.html')
        .pipe(gulp.dest('dist'));
});

gulp.task('default', [
    'compile-font-awesome',
    'copy-font-awesome',
    'copy-images',
    'resize-images',
    'copy-index'
]);