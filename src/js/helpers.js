/**
 * Capitalize the first letter of the word.
 * 
 * @param  string value
 * @return string
 */
function ucfirst(value) {
    if(value) {
        return value[0].toUpperCase() + value.slice(1);
    }
}

export {ucfirst};