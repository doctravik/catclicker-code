/**
 * Usage 
 * 1. Add callback parameter (callback=?) to the url
 * 2. Make request to the endpoint
 *  
 * Jsonp.fetch(url)
 *     .then(onSuccess)
 *     .catch(onError);
 */

class Jsonp
{
    constructor(url) 
    {
        this.url = url;
    }

    /**
     * Create new instance of Jsonp and send request to the endpoint
     *
     * @return Promise
     */
    static fetch(url)
    {
        return (new Jsonp(url)).fetchData();
    }

    /**
     * Send request to the endpoint.
     * 
     * @return Promise
     */
    fetchData() {
        // Uses native Promise. Older browsers need polyfill.
        if (!window.Promise) {
            throw 'Promise not available. Use a polyfill!';
        }

        return new Promise((resolve, reject) => {
            let callbackName = this.createCallback();

            let script = this.createScript();

            this.resolvePromise(callbackName, resolve);
                      
            script.onerror = () => {
                reject(Error('Error during request to ' + this.url));
            }

            script.onload = this.cleanCallbacks.bind(this, callbackName, script);
        });
    }

    /**
     * Build request string
     *
     * @return string callbackName
     */
    createCallback()
    {
        let callbackName = this.createCallbackName();

        this.insertCallbackIntoUrl(callbackName);

        return callbackName;
    }

    /**
     * Create the name of the callback function
     *
     * @return string
     */
    createCallbackName()
    {
        return 'jsonp_callback_' + Math.round(100000 * Math.random());
    }

    /**
     * Insert the name of the callback function into the url request.
     *
     * @return void
     */
    insertCallbackIntoUrl(callbackName)
    {
        if (/callback=?/.test(this.url)) {
            this.url = this.url.replace('=?', '=' + callbackName);
        } else {
            throw("Callback parameter is required. But it couldn't be found.");
        }
    }

    /**
     * Resolve the promise.
     *
     * @return void
     */
    resolvePromise(callbackName, resolve)
    {
        window[callbackName] = (data) => { resolve(data); };
    }

    /**
     * Insert script into the client page.
     *
     * @return HTMLElement
     */
    createScript()
    {
        let script = document.createElement('script');
        
        script.src = this.url;

        document.body.appendChild(script);
        
        return script;
    }

    /**
     * Clean enviroment when the job will be done.
     *
     * @return void
     */
    cleanCallbacks(callbackName, script) 
    {
        this.removeCallback(callbackName);

        // remove script tag from the client page
        document.body.removeChild(script);
    }

    /**
     * Remove executed callback function from the global object.
     *
     * @return void
     */
    removeCallback(callbackName) 
    {
        window[callbackName] = null;

        delete window[callbackName];
    }
}

export default Jsonp;