class CatLocalStorage
{
    constructor(cats = []) 
    {
        if (!localStorage.cats) {
            this.setCats(cats);
        }
    }

    getCats() 
    {
        return JSON.parse(localStorage.cats);
    }

    setCats(cats)
    {
        localStorage.cats = JSON.stringify(cats);
    }

    unsetCats()
    {
        localStorage.removeItem('cats');
    }
}

export default CatLocalStorage;