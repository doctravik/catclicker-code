class CatStorage
{
    constructor(cats = []) 
    {
        this.cats = JSON.stringify(cats);
    }

    getCats() 
    {
        return JSON.parse(this.cats);
    }

    setCats(cats)
    {
        this.cats = JSON.stringify(cats);
    }

    unsetCats()
    {
        this.cats = [];
    }
}

export default CatStorage;