// css
import 'normalize.css';
import './../../dist/css/font-awesome/font-awesome.css';
import './../css/app.css';

import Vue from 'vue';
import App from './App.vue';

window.bus = new Vue();
window.CatClicker = {
    jsonpCallbacks:  {}
};

new Vue({
    el: '#app',
    render: h => h(App)
});