export default {
    cats: [
        {
            id: 1,
            name: 'Feline looking cute',
            src: 'img/cats/cat-feline-looking-cute-sitting.jpg',
            link: 'https://pixabay.com/en/cat-feline-looking-cute-sitting-1578538/',
            counter: 0,
            thumbnail: 'img/avatars/cat-feline-looking-cute-sitting.jpg'
        }, {
            id: 2,
            name: 'Autumn young cat',
            src: 'img/cats/cat-kitten-autumn-young-cat.jpg',
            link: 'https://pixabay.com/en/cat-kitten-autumn-young-cat-1034090/',
            counter: 0,
            thumbnail: 'img/avatars/cat-kitten-autumn-young-cat.jpg'
        }, {
            id: 3,
            name: 'Mackerel fluffy',
            src: 'img/cats/cat-black-and-white-mackerel-fluffy.jpg',
            link: 'https://pixabay.com/en/cat-black-and-white-mackerel-fluffy-514319/',
            counter: 0,
            thumbnail: 'img/avatars/cat-black-and-white-mackerel-fluffy.jpg'
        }, {
            id: 4,
            name: 'Cute domestic',
            src: 'img/cats/cat-pet-animal-cute-domestic.jpg',
            link: 'https://pixabay.com/en/cat-pet-animal-cute-domestic-691591/',
            counter: 0,
            thumbnail: 'img/avatars/cat-pet-animal-cute-domestic.jpg'
        }, {
            id: 5,
            name: 'Selkirk straight blue',
            src: 'img/cats/cat-young-cat-selkirk-straight-blue.jpg',
            link: 'https://pixabay.com/en/cat-young-cat-selkirk-straight-blue-665126/',
            counter: 0,
            thumbnail: 'img/avatars/cat-young-cat-selkirk-straight-blue.jpg'
        }
    ],

    levels: {
        'newborn': [0,1],
        'infant': [2,3],
        'child': [4,5],
        'teen': [6,7],
        'adult': [8,9],
    }
}