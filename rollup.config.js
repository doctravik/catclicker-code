import replace from 'rollup-plugin-replace';
import vue2 from 'rollup-plugin-vue2';
import postcss from 'rollup-plugin-postcss';
import buble from 'rollup-plugin-buble';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import uglify from 'rollup-plugin-uglify';
import cssnext from 'postcss-cssnext';
import cssnano from 'cssnano';

export default {
    entry: 'src/js/app.js',
    dest: 'dist/js/bundle.js',
    format: 'iife',
    plugins: [
        vue2(),
        postcss({
            plugins: [
                cssnext(),
                cssnano({
                    autoprefixer: false
                }),
            ]
        }),
        buble({
            exclude: "node_modules/**"
        }),
        resolve({
            jsnext: true,
            main: true,
            browser: true,
        }),
        commonjs({
            include: 'node_modules/**'
        }),
        replace({
            exclude: 'node_modules/**',
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
            'process.env.VUE_ENV': JSON.stringify(process.env.VUE_ENV || 'browser')
        }),
        (process.env.NODE_ENV === 'production' && uglify()),
    ],
};